<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <b>Sign Up Form</b><br><br>
    <form action="/welcome" method="POST">
        @csrf
        <label for="FN">First Name:</label><br><br>
        <input type="text" id="FN" name ="nama_depan"><br><br>
        <label for="LN">Last Name:</label><br><br>
        <input type="text" id="LN" name = "nama_belakang"><br><br>
        <label for="gender">Gender:</label><br><br>
        <input type="radio" name="gender">Male <br> 
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br><br>
        <label for="nationality">Nationality:</label><br><br>
        <select name="nationality" id="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Japanesen">Japanesen</option>
            <option value="Korean">Korean</option>
            <option value="Chinese">Chinese</option>
            <option value="Other">Other</option>
        </select><br><br>
        <label for="Language">Language Spoken:</label><br><br>
        <input type="checkbox" name="Bahasa" id="Language">Bahasa Indonesia<br>
        <input type="checkbox" name="English" id="Language">English<br>
        <input type="checkbox" name="Other" id="Language">Other<br><br>
        <label for="bio">Bio:</label><br><br>
        <textarea name="Bio" id="bio" cols="30" rows="10"></textarea><br><br>
        <button type="submit" name="submit">Sign Up</button>        
    </form>
</body>
</html>