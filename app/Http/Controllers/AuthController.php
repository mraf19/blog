<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('form');
    }
    public function welcome(Request $request){
        // dd($request->all());
        $nama = $request["nama_depan"]. " ". $request["nama_belakang"];
        return view('welcome', compact('nama'));
        }
}
